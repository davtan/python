#! /usr/bin/env python
import math

''' The number deficiency is n - sigma(n) where n is a integer number 
and sigma (n) is the sum of the divisors of number n excluding n'''

def calcDef(number):
	'Calculates the number deficiency'
	sumDivisors = 1
	for divisor in range(2, number):
		if (number % divisor) == 0:
			sumDivisors += divisor
	return ( number - sumDivisors )

def determineDef (number):
	'Determines number deficiency and displays them'
	deficiency = calcDef( number )
	displayDef( number, deficiency )

def displayDef (number, deficiency):
	'Displays the number in a format based on defiicency'
	if deficiency == 0:
		print "{number} neither".format (number=number)
	elif deficiency > 0:
		print "{number} deficient".format (number=number)
	else:
		print "{number} abundant by {abundance}".format ( number=number, abundance=deficiency*-1 )

def getNumbers(filename):
	'Gets a list of integer numbers from a file'
	numberFile = open(filename, 'r')
	numbers = []
	numString = numberFile.readline()	
	while ( numString != ''):
		if validateInput(numString):
			number = int (numString)
			numbers.append (number)
		numString = numberFile.readline()	
	return numbers

def determineDefOfNumbers (numbers):
	'Determines the deficiencies of each number in the list and displays them'
	for number in numbers:
		determineDef(number)

def validateInput(inputNum):
	try:
		return int (inputNum) > 0
	except:
		return false

filename="numbers.txt"
numbers=getNumbers(filename)
determineDefOfNumbers(numbers)
