
def gameOfThree(number):
	while number != 1:
		remainder = number % 3
		if remainder == 2:
			additionNum = 1
		elif remainder == 1:
			additionNum = -1
		else:
			additionNum = 0
		print "{number} {additionNum}".format (number=number,additionNum=additionNum)
		number = (number + additionNum) / 3

def getNumber():
	numberStr = raw_input ("Please enter a number greater than zero\n")
	while not validateInput(numberStr):
		numberStr = raw_input ("Please enter a number greater than zero\n")
	number = int (numberStr)
	return number

def validateInput(inputNum):
	try:
		return int (inputNum) > 0
	except:
		return false

number=getNumber()
gameOfThree(number)