def funnyPlant(peopleToFeed,startingFruits):
 #this list consists of the number of fruit producing index amount of fruits 
 fruitProduction=[startingFruits]
 fruitsToFeed=startingFruits
 while fruitsToFeed < peopleToFeed:
  fruitsToFeed=checkFruitProd(fruitProduction)
  fruitProduction.insert(0,fruitsToFeed)
 return len (fruitProduction)

def checkFruitProd(fruitProduction):
 fruitsProduced = 0
 for fruitProduced,plants in enumerate (fruitProduction):
  fruitsProduced += (fruitProduced+1) * plants
 return fruitsProduced

def validateInput(inputNum):
 try:
  return int (inputNum) > 0
 except:
  return False

def getInput():
 inputStr=raw_input("Please enter num of people to feed and num of starting fruits\n")
 while (inputStr != ""):
  tokens = inputStr.split(" ")
  if (len(tokens) == 2):
   startingPeople = tokens[0]
   startingFruits = tokens[1]
   if ( validateInput(startingPeople) and validateInput(startingFruits)):
      return int (startingPeople),int (startingFruits)
  inputStr=raw_input("Please enter num of people to feed and num of starting fruits\n")
 return "",""


startingPeople,startingFruits=getInput()
if (startingPeople!=""):
 print funnyPlant(startingPeople,startingFruits)
