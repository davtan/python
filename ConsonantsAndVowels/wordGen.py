from random import randint

lowerVowels = ('a','e','i','o','u')
upperVowels = ('A','E','I','O','U')
lowerConsonants = ('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z')
upperConsonants = ('B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z')

def wordGen (inputString,placeholders,replaceCharTuples):
	'generates a word by replacing placeholders'
	charList = list (inputString)
	if inputString != "":
		# replace each character in order
		for i,char in enumerate (inputString):
			if char in placeholders:
				# find the replace array that corresponds to the placeholder
				indexOfTuple = placeholders.index (char)
				charList[i] = pickRandomChar (replaceCharTuples[indexOfTuple])
	return "".join(charList)

def pickRandomChar (charArray):
	return charArray[ randint (0, len (charArray) - 1)]


placeholders=('v','c','V','C')
replaceCharTuples=(lowerVowels,lowerConsonants,upperVowels,upperConsonants)
inputString = raw_input ("")
word = wordGen(inputString,placeholders,replaceCharTuples)
print word